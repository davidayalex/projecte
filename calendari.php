<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
</head>
<body onload="calendari()">
<script type="text/javascript">
function calendari(data) {
if(data == null)
  data = new Date();
  dia = data.getDate();
  mes = data.getMonth();
  any = data.getFullYear();

  aquest_mes = new Date(any, mes);
  proper_mes = new Date(any, mes + 1, 1);

  mesos = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre');

  primer_dia = aquest_mes.getDay();
  dies_del_mes = Math.round((proper_mes.getTime() - aquest_mes.getTime()) / (1000 * 60 * 60 * 24));

  calendario_html = '<table style="border: 1px solid black; background-color: white; color: orange;">';
  calendario_html += '<tr><td colspan="7" style="border: 1px solid black; background-color: white; color: orange; text-align: center;">' + dia + ' ' + mesos[mes] + ' ' + any + '</td></tr>';
  calendario_html += '<tr>';

  for(dia_setmana = 0; dia_setmana < primer_dia; dia_setmana++) {
    calendario_html += '<td style="border: 1px solid black; background-color: white; color:000000; text-align: center;"> </td>';
  }

  dia_setmana = primer_dia;
  for(contar_dia = 1; contar_dia <= dies_del_mes; contar_dia++) {
    dia_setmana %= 7;
    if(dia_setmana == 0)
      calendario_html += '<tr> </tr>';
    if(dia == contar_dia)
      calendario_html += '<div class="cdia"><td style="border: 1px solid black; text-align: center;"><b>' + contar_dia + '</b></td></div>';
    else
      calendario_html += '<div><td style="border: 1px solid black; text-align: center;"> ' + contar_dia + ' </td></div>';
    dia_setmana++;
  }

  calendario_html += '</tr>';
  calendario_html += '</table>';

  document.write(calendario_html);
}
</script>
</body>
