# Instrucciones de instalación
  - Hacer un pull en la carpeta donde este apuntado apache
# Creación de base de datos:
En la carpeta proyecte se encuentra el fichero de creación de la base de datos : "base de datos.txt"
  - Lo primero es entrar en sql con el comando mysql -u "usuario" -p y poner la contraseña donde "usuario es tu nombre de usuario"
  - Después copiar todas las líneas del fichero de texto y pegarlas en la consola (todas a la vez), si ya tienes una base de datos creada elimina la primera línea y en "USE agenda" borra agenda y pon tu base de datos

# Puesta en marcha 
    - Cambiar las variables del fichero conexion.php
	    $server="localhost"; servidor donde se encuentra la base de datos
    	$user="root"; usuario de la base de datos
    	$contrasena="unacontraseña"; contraseña de la base de datos
    	$basedatos="agenda"; base de datos
    - apuntar tu servidor al fichero "login.php"
# Usuarios de prueba:
- usuario: alex 
 //Contraseña: 1234
- usuario: david 
//Contraseña:1234