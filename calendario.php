<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
</head>
<body class="bodyCal" onload="calendari()">
  <div id="calendario"></div>
  <link rel="stylesheet" type="text/css" href="./css/ceuser.css"/>
<script type="text/javascript">
mes=0;
data = new Date();
function calendari() {
    dia = data.getDate();
    mes = data.getMonth();
    any = data.getFullYear();

  aquest_mes = new Date(any, mes, 1);
  proper_mes = new Date(any, mes + 1, 1);

  mesos = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre');

  primer_dia = aquest_mes.getDay();
  dies_del_mes = Math.round((proper_mes.getTime() - aquest_mes.getTime()) / (1000 * 60 * 60 * 24));

  calendario_html = '<table id="tCal">';
  calendario_html += '<tr><td id="tdCal" colspan="7"><span id="menor" onclick="resmes()"> < </span>' + mesos[mes] + '<span id="major" onclick="summes()"> > </span> <span onclick="anomenos()"> < </span>' + any + '<span onclick="anomas()"> > </span></td></tr>';
  calendario_html += '<tr>';

  for(dia_setmana = 0; dia_setmana < primer_dia; dia_setmana++) {
    calendario_html += '<td id="tdCal"></td>';
  }

  dia_setmana = primer_dia;
  for(contar_dia = 1; contar_dia <= dies_del_mes; contar_dia++) {
    dia_setmana %= 7;
    if(dia_setmana == 0)
      calendario_html += '<tr> </tr>';
    if(dia == contar_dia)
      calendario_html += '<div class="cdia"><td id="cgdia" onclick="cogerdia('+ contar_dia +','+ mes +','+ any +')"><b>' + contar_dia + '</b></td></div>';
    else
      calendario_html += '<div class="cdia"><td id="cgdia" onclick="cogerdia('+ contar_dia +','+ mes +','+ any +')"> ' + contar_dia + ' </td></div>';
    dia_setmana++;
  }

  calendario_html += '</tr>';
  calendario_html += '</table>';

  document.getElementById("calendario").innerHTML = calendario_html;
}

function summes(){
  data.setMonth(data.getMonth() + 1);
  calendari();
  }

function resmes(){
  data.setMonth(data.getMonth() - 1);
  calendari();
  }

function anomas(){
  data.setFullYear(data.getFullYear() + 1);
  calendari();
  }

function anomenos(){
  data.setFullYear(data.getFullYear() - 1);
  calendari();
  }

function cogerdia(contar_dia, mes, any){
  mes = mes + 1;
  parent.document.getElementsByName("fecha")[0].value=contar_dia+"/"+mes+"/"+any;
  parent.document.getElementsByName("iframecalendar")[0].style.display="none";
}
</script>
</body>
